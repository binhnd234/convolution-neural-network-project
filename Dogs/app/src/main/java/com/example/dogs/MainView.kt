package com.example.dogs

import android.app.Activity
import android.app.SearchManager
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.example.dogs.databinding.ActivityMainBinding
import com.theartofdev.edmodo.cropper.CropImage
import java.io.BufferedReader
import java.io.InputStreamReader

class MainView : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var model: CNNModel
    private var labels = mutableListOf<String>()
    private var index = 0

    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        model = CNNModel()

        getLabel()

        binding.button.setOnClickListener {
            if (model.isDataLoading()) {
                Toast.makeText(
                    this, "Model is loading, please wait!", Toast.LENGTH_SHORT
                ).show()
            } else {
                CropImage.activity().start(this)
            }
        }
        binding.search.setOnClickListener {
            search()
        }
        model.resultMap.observe(this, Observer { map ->
            displayResult(map)
        })
    }

    private fun prepareInput(uri: Uri) {
        val imageBitmap = if (Build.VERSION.SDK_INT >= 29) {
            ImageDecoder.decodeBitmap(ImageDecoder.createSource(contentResolver, uri))
        } else {
            MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
        }

        val inputBitmap = Bitmap.createScaledBitmap(imageBitmap, 224, 224, true)
        val batchNum = 0
        val input = Array(1) { Array(224) { Array(224) { FloatArray(3) } } }
        for (x in 0..223) {
            for (y in 0..223) {
                val pixel = inputBitmap.getPixel(x, y)
                input[batchNum][x][y][0] = Color.red(pixel) / 255.0f
                input[batchNum][x][y][1] = Color.green(pixel) / 255.0f
                input[batchNum][x][y][2] = Color.blue(pixel) / 255.0f
            }
        }
        Log.d(TAG, input[0][0][0][0].toString())
        model.predictImage(input)
    }

    private fun displayResult(map: HashMap<String, Any>) {
        index = map["index"] as Int
        val confidence: Float = map["confidence"] as Float
        val text = String.format("%s: %.4f%%", labels[index], confidence)

        binding.label.text = text
        binding.resultText.visibility = View.VISIBLE
    }

    private fun search() {
        val intent = Intent(Intent.ACTION_WEB_SEARCH).apply {
            putExtra(SearchManager.QUERY, labels[index] + " dog")
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    private fun getLabel() {
        val reader = BufferedReader(InputStreamReader(assets.open("stanford/labels.txt")))
        for (i in 0..119) {
            val item = reader.readLine()
            labels.add(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                binding.resultText.visibility = View.GONE
                val uri = result.uri
                binding.label.text = uri.toString()
                binding.image.setImageURI(uri)
                prepareInput(uri)
            }
        }
    }
}