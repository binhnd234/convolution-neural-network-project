package com.example.dogs

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions
import com.google.firebase.ml.common.modeldownload.FirebaseModelManager
import com.google.firebase.ml.custom.*
import kotlin.math.exp

class CNNModel() {
    private lateinit var remoteModel: FirebaseCustomRemoteModel
    private lateinit var localModel: FirebaseCustomLocalModel
    private lateinit var conditions: FirebaseModelDownloadConditions
    private lateinit var options: FirebaseModelInterpreterOptions
    private lateinit var inputOutputOptions: FirebaseModelInputOutputOptions
    private var interpreter: FirebaseModelInterpreter? = null
    private var isLoading: Boolean = false
    val resultMap = MutableLiveData<HashMap<String, Any>>()

    companion object {
        private const val TAG = "CNNModel"
    }

    init {
        getModel()
        inputOutputOptions = FirebaseModelInputOutputOptions.Builder()
            .setInputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, 224, 224, 3))
            .setOutputFormat(0, FirebaseModelDataType.FLOAT32, intArrayOf(1, 120))
            .build()
    }

    private fun getModel() {
        if (!isLoading && interpreter == null) {
            isLoading = true

            remoteModel = FirebaseCustomRemoteModel.Builder("stanford_dogs_model_3").build()
            localModel = FirebaseCustomLocalModel.Builder()
                .setAssetFilePath("stanford/model.tflite")
                .build()
            conditions = FirebaseModelDownloadConditions.Builder()
                .requireWifi()
                .build()

            FirebaseModelManager.getInstance().download(remoteModel, conditions)
                .addOnCompleteListener {
                    Log.d(TAG, "Download successfully")
                }

            FirebaseModelManager.getInstance().isModelDownloaded(remoteModel)
                .addOnSuccessListener { isDownloaded ->
                    if (isDownloaded) {
                        Log.d(TAG, "Use remote model")
                        options = FirebaseModelInterpreterOptions.Builder(remoteModel).build()
                    } else {
                        Log.d(TAG, "Use local model")
                        options = FirebaseModelInterpreterOptions.Builder(localModel).build()
                    }
                    interpreter = FirebaseModelInterpreter.getInstance(options)
                    isLoading = false
                }
                .addOnFailureListener {
                    isLoading = false
                }
        }
    }

    fun predictImage(input: Array<Array<Array<FloatArray>>>) {
        val inputs = FirebaseModelInputs.Builder()
            .add(input)
            .build()
        interpreter?.let { firebaseModelInterpreter ->
            firebaseModelInterpreter.run(inputs, inputOutputOptions)
                .addOnSuccessListener { result ->
                    val output = result.getOutput<Array<FloatArray>>(0)
                    val probabilities = output[0]
                    processOutput(probabilities)
                }
                .addOnFailureListener {
                    Log.d(TAG, it.toString())
                }
        }
    }

    private fun processOutput(array: FloatArray) {
        val maxIndex = array.max()?.let { array.indexOf(it) } ?: 0
        Log.d(TAG, maxIndex.toString())

        var sum = 0f
        for (i in array.indices) {
            sum += exp(array[i])
        }

        val map = HashMap<String, Any>()
        map["index"] = maxIndex
        map["confidence"] = exp(array[maxIndex]) * 100 / sum

        resultMap.value = map
    }

    fun isDataLoading(): Boolean {
        return isLoading
    }
}